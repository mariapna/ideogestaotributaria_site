-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 20-Dez-2018 às 12:51
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_ideogestaotributaria_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_commentmeta`
--

DROP TABLE IF EXISTS `id_commentmeta`;
CREATE TABLE IF NOT EXISTS `id_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_comments`
--

DROP TABLE IF EXISTS `id_comments`;
CREATE TABLE IF NOT EXISTS `id_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_comments`
--

INSERT INTO `id_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-12-17 10:36:50', '2018-12-17 12:36:50', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_links`
--

DROP TABLE IF EXISTS `id_links`;
CREATE TABLE IF NOT EXISTS `id_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_options`
--

DROP TABLE IF EXISTS `id_options`;
CREATE TABLE IF NOT EXISTS `id_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=268 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_options`
--

INSERT INTO `id_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/ideogestaotributaria_site', 'yes'),
(2, 'home', 'http://localhost/projetos/ideogestaotributaria_site', 'yes'),
(3, 'blogname', 'Ideo Gestão Tributária', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'agenciahcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:115:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:23:\"base-ideo/base-ideo.php\";i:2;s:21:\"meta-box/meta-box.php\";i:3;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ideo', 'yes'),
(41, 'stylesheet', 'ideo', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '43764', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'id_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1545313010;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1545352610;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1545395830;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1545395867;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1545051393;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(124, '_site_transient_timeout_browser_7c536d82012ce7c421315e5571540a1e', '1545655031', 'no'),
(125, '_site_transient_browser_7c536d82012ce7c421315e5571540a1e', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"70.0.3538.110\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(129, 'can_compress_scripts', '1', 'no'),
(262, '_site_transient_timeout_theme_roots', '1545311390', 'no'),
(263, '_site_transient_theme_roots', 'a:1:{s:4:\"ideo\";s:7:\"/themes\";}', 'no'),
(143, 'current_theme', 'ideo', 'yes'),
(144, 'theme_mods_ideo', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:4:\"ideo\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(145, 'theme_switched', '', 'yes'),
(148, 'recently_activated', 'a:0:{}', 'yes'),
(167, 'category_children', 'a:0:{}', 'yes'),
(175, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(219, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:8:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";s:8:\"destaque\";s:4:\"show\";s:10:\"quem_somos\";s:4:\"show\";s:8:\"servicos\";s:4:\"show\";s:8:\"clientes\";s:4:\"show\";s:9:\"parceiros\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(266, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1545309594;s:7:\"checked\";a:1:{s:4:\"ideo\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(267, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1545309595;s:7:\"checked\";a:16:{s:19:\"akismet/akismet.php\";s:3:\"4.1\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.3.8\";s:23:\"base-ideo/base-ideo.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:3:\"5.1\";s:32:\"disqus-comment-system/disqus.php\";s:6:\"3.0.16\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.2\";s:41:\"google-tag-manager/google-tag-manager.php\";s:5:\"1.0.2\";s:9:\"hello.php\";s:5:\"1.7.1\";s:28:\"wysija-newsletters/index.php\";s:6:\"2.10.2\";s:21:\"meta-box/meta-box.php\";s:6:\"4.15.9\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.3.9\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:27:\"svg-support/svg-support.php\";s:6:\"2.3.15\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"1.4.1\";s:27:\"wp-super-cache/wp-cache.php\";s:5:\"1.6.4\";s:24:\"wordpress-seo/wp-seo.php\";s:5:\"9.2.1\";}s:8:\"response\";a:3:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:7:\"4.3.8.3\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:147:\"<p>Read the <a href=\"https://contactform7.com/category/releases/\">release announcement post</a> before upgrading. There is an important notice.</p>\";s:6:\"tested\";s:5:\"5.0.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"9.3\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wordpress-seo.9.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.0.1\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:7:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"4.1\";s:7:\"updated\";s:19:\"2018-12-08 12:30:17\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/translation/plugin/akismet/4.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-02-03 16:12:23\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.2.2\";s:7:\"updated\";s:19:\"2018-02-15 18:31:27\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.2.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"2.10.2\";s:7:\"updated\";s:19:\"2018-10-16 12:30:39\";s:7:\"package\";s:86:\"https://downloads.wordpress.org/translation/plugin/wysija-newsletters/2.10.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.15.9\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.15.9/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:5;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"0.10.1\";s:7:\"updated\";s:19:\"2017-04-14 19:57:38\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/wp-mail-smtp/0.10.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:6;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"6.3.1\";s:7:\"updated\";s:19:\"2018-02-03 16:01:18\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/6.3.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:12:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"4.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:6:\"3.0.16\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.16.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:2:\"1x\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"google-tag-manager/google-tag-manager.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/google-tag-manager\";s:4:\"slug\";s:18:\"google-tag-manager\";s:6:\"plugin\";s:41:\"google-tag-manager/google-tag-manager.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/google-tag-manager/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/google-tag-manager.1.0.2.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:62:\"https://s.w.org/plugins/geopattern-icon/google-tag-manager.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:6:\"2.10.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.10.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:2:\"1x\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.15.9\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.15.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.9\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"svg-support/svg-support.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/svg-support\";s:4:\"slug\";s:11:\"svg-support\";s:6:\"plugin\";s:27:\"svg-support/svg-support.php\";s:11:\"new_version\";s:6:\"2.3.15\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/svg-support/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/svg-support.2.3.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:64:\"https://ps.w.org/svg-support/assets/icon-256x256.png?rev=1417738\";s:2:\"1x\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";s:3:\"svg\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/svg-support/assets/banner-1544x500.jpg?rev=1215377\";s:2:\"1x\";s:66:\"https://ps.w.org/svg-support/assets/banner-772x250.jpg?rev=1215377\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"1.4.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.1.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=1982773\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=1982773\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"wp-super-cache/wp-cache.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-super-cache\";s:4:\"slug\";s:14:\"wp-super-cache\";s:6:\"plugin\";s:27:\"wp-super-cache/wp-cache.php\";s:11:\"new_version\";s:5:\"1.6.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-super-cache/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-super-cache.1.6.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-256x256.png?rev=1095422\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-super-cache/assets/icon-128x128.png?rev=1095422\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-super-cache/assets/banner-1544x500.png?rev=1082414\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-super-cache/assets/banner-772x250.png?rev=1082414\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(220, 'CPT_configured', 'TRUE', 'yes'),
(265, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.0.2-partial-1.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.0.1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.0.2-partial-1.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-rollback-1.zip\";}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.0.1\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1545309593;s:15:\"version_checked\";s:5:\"5.0.1\";s:12:\"translations\";a:0:{}}', 'no'),
(177, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(179, 'configuracao', 'a:19:{s:8:\"last_tab\";s:0:\"\";s:14:\"logo_principal\";a:9:{s:3:\"url\";s:97:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo-principal.png\";s:2:\"id\";s:2:\"11\";s:6:\"height\";s:3:\"574\";s:5:\"width\";s:4:\"1087\";s:9:\"thumbnail\";s:105:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo-principal-150x150.png\";s:5:\"title\";s:14:\"logo-principal\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:9:\"img_fundo\";a:9:{s:3:\"url\";s:102:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\";s:2:\"id\";s:2:\"25\";s:6:\"height\";s:4:\"1080\";s:5:\"width\";s:4:\"1674\";s:9:\"thumbnail\";s:110:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal-150x150.png\";s:5:\"title\";s:19:\"back-logo-principal\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"titulo_porque_aideo\";s:14:\"porque a ideo?\";s:18:\"texto_porque_aideo\";s:96:\"IDEO, tem por objetivo fornecer soluções lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ips\";s:19:\"texto_porque_aideo2\";s:143:\"lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum\";s:13:\"titulo_footer\";s:19:\"ta esperando o que?\";s:16:\"img_fundo_footer\";a:9:{s:3:\"url\";s:94:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-footer.png\";s:2:\"id\";s:2:\"41\";s:6:\"height\";s:3:\"941\";s:5:\"width\";s:4:\"1915\";s:9:\"thumbnail\";s:102:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-footer-150x150.png\";s:5:\"title\";s:11:\"back-footer\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"telefone_footer\";s:15:\"(11) 92345-6789\";s:29:\"redes_sociais_footer_whatsapp\";s:1:\"#\";s:30:\"redes_sociais_footer_instagram\";s:1:\"#\";s:29:\"redes_sociais_footer_facebook\";s:1:\"#\";s:26:\"redes_sociais_footer_email\";s:1:\"#\";s:29:\"redes_sociais_footer_linkedin\";s:1:\"#\";s:28:\"redes_sociais_footer_twitter\";s:1:\"#\";s:14:\"logo_side_menu\";a:9:{s:3:\"url\";s:87:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo.png\";s:2:\"id\";s:2:\"59\";s:6:\"height\";s:3:\"147\";s:5:\"width\";s:3:\"259\";s:9:\"thumbnail\";s:95:\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo-150x147.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:13:\"desc_clientes\";s:141:\"Nossos clientes atuam no desenvolvimento, fabricação, distribuição e comercialização de produtos, bem como na prestação de serviços.\";s:14:\"desc_parceiros\";s:91:\"Nossos parceiros de negócios, lorem ipsum porem ipsum porem ipsum lorem ipsum lorem ipsum.\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(180, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1545154672;}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_postmeta`
--

DROP TABLE IF EXISTS `id_postmeta`;
CREATE TABLE IF NOT EXISTS `id_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=324 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_postmeta`
--

INSERT INTO `id_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1545050147:1'),
(4, 6, '_edit_lock', '1545054831:1'),
(5, 7, '_edit_lock', '1545068499:1'),
(6, 7, '_wp_page_template', 'pages/inicial.php'),
(7, 9, '_edit_lock', '1545056177:1'),
(8, 1, '_edit_lock', '1545056645:1'),
(9, 11, '_wp_attached_file', '2018/12/logo-principal.png'),
(10, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1087;s:6:\"height\";i:574;s:4:\"file\";s:26:\"2018/12/logo-principal.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"logo-principal-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"logo-principal-300x158.png\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"logo-principal-768x406.png\";s:5:\"width\";i:768;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"logo-principal-1024x541.png\";s:5:\"width\";i:1024;s:6:\"height\";i:541;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(74, 41, '_wp_attached_file', '2018/12/back-footer.png'),
(12, 21, '_edit_lock', '1545064621:1'),
(13, 22, '_edit_lock', '1545064678:1'),
(14, 23, '_edit_lock', '1545064704:1'),
(15, 24, '_edit_lock', '1545064796:1'),
(16, 25, '_wp_attached_file', '2018/12/back-logo-principal.png'),
(17, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1674;s:6:\"height\";i:1080;s:4:\"file\";s:31:\"2018/12/back-logo-principal.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"back-logo-principal-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"back-logo-principal-300x194.png\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"back-logo-principal-768x495.png\";s:5:\"width\";i:768;s:6:\"height\";i:495;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"back-logo-principal-1024x661.png\";s:5:\"width\";i:1024;s:6:\"height\";i:661;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(18, 29, '_wp_attached_file', '2018/12/hello.png'),
(19, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:17:\"2018/12/hello.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(20, 35, '_menu_item_type', 'custom'),
(21, 35, '_menu_item_menu_item_parent', '0'),
(22, 35, '_menu_item_object_id', '35'),
(23, 35, '_menu_item_object', 'custom'),
(24, 35, '_menu_item_target', ''),
(25, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(26, 35, '_menu_item_xfn', ''),
(27, 35, '_menu_item_url', '#home'),
(29, 36, '_menu_item_type', 'custom'),
(30, 36, '_menu_item_menu_item_parent', '0'),
(31, 36, '_menu_item_object_id', '36'),
(32, 36, '_menu_item_object', 'custom'),
(33, 36, '_menu_item_target', ''),
(34, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(35, 36, '_menu_item_xfn', ''),
(36, 36, '_menu_item_url', '#quem-somos'),
(38, 37, '_menu_item_type', 'custom'),
(39, 37, '_menu_item_menu_item_parent', '0'),
(40, 37, '_menu_item_object_id', '37'),
(41, 37, '_menu_item_object', 'custom'),
(42, 37, '_menu_item_target', ''),
(43, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(44, 37, '_menu_item_xfn', ''),
(45, 37, '_menu_item_url', '#servicos'),
(47, 38, '_menu_item_type', 'custom'),
(48, 38, '_menu_item_menu_item_parent', '0'),
(49, 38, '_menu_item_object_id', '38'),
(50, 38, '_menu_item_object', 'custom'),
(51, 38, '_menu_item_target', ''),
(52, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(53, 38, '_menu_item_xfn', ''),
(54, 38, '_menu_item_url', '#clientes'),
(56, 39, '_menu_item_type', 'custom'),
(57, 39, '_menu_item_menu_item_parent', '0'),
(58, 39, '_menu_item_object_id', '39'),
(59, 39, '_menu_item_object', 'custom'),
(60, 39, '_menu_item_target', ''),
(61, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(62, 39, '_menu_item_xfn', ''),
(63, 39, '_menu_item_url', '#parceiros'),
(75, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1915;s:6:\"height\";i:941;s:4:\"file\";s:23:\"2018/12/back-footer.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"back-footer-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"back-footer-300x147.png\";s:5:\"width\";i:300;s:6:\"height\";i:147;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"back-footer-768x377.png\";s:5:\"width\";i:768;s:6:\"height\";i:377;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"back-footer-1024x503.png\";s:5:\"width\";i:1024;s:6:\"height\";i:503;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(65, 40, '_menu_item_type', 'custom'),
(66, 40, '_menu_item_menu_item_parent', '0'),
(67, 40, '_menu_item_object_id', '40'),
(68, 40, '_menu_item_object', 'custom'),
(69, 40, '_menu_item_target', ''),
(70, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(71, 40, '_menu_item_xfn', ''),
(72, 40, '_menu_item_url', '#contato'),
(76, 43, '_edit_lock', '1545136171:1'),
(77, 42, '_edit_lock', '1545135962:1'),
(78, 44, '_edit_lock', '1545136415:1'),
(79, 45, '_edit_lock', '1545136638:1'),
(80, 46, '_edit_lock', '1545141913:1'),
(81, 47, '_wp_attached_file', '2018/12/profissional1.png'),
(82, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:810;s:6:\"height\";i:540;s:4:\"file\";s:25:\"2018/12/profissional1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"profissional1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"profissional1-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"profissional1-768x512.png\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(83, 46, '_thumbnail_id', '47'),
(84, 48, '_edit_lock', '1545144512:1'),
(85, 49, '_wp_attached_file', '2018/12/profissional2.png'),
(86, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:810;s:6:\"height\";i:540;s:4:\"file\";s:25:\"2018/12/profissional2.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"profissional2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"profissional2-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"profissional2-768x512.png\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(87, 48, '_thumbnail_id', '49'),
(88, 50, '_edit_lock', '1545141833:1'),
(89, 51, '_wp_attached_file', '2018/12/servico1.png'),
(90, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:124;s:4:\"file\";s:20:\"2018/12/servico1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(91, 50, '_thumbnail_id', '51'),
(92, 52, '_edit_lock', '1545141751:1'),
(93, 53, '_wp_attached_file', '2018/12/servico2.png'),
(94, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:119;s:6:\"height\";i:109;s:4:\"file\";s:20:\"2018/12/servico2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(95, 52, '_thumbnail_id', '53'),
(96, 54, '_edit_lock', '1545141746:1'),
(97, 55, '_wp_attached_file', '2018/12/servico3.png'),
(98, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:126;s:6:\"height\";i:131;s:4:\"file\";s:20:\"2018/12/servico3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(99, 54, '_thumbnail_id', '55'),
(100, 56, '_edit_lock', '1545141739:1'),
(101, 57, '_wp_attached_file', '2018/12/servico4.png'),
(102, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:119;s:6:\"height\";i:111;s:4:\"file\";s:20:\"2018/12/servico4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(103, 56, '_thumbnail_id', '51'),
(104, 58, '_edit_lock', '1545141693:1'),
(105, 58, '_thumbnail_id', '57'),
(106, 59, '_wp_attached_file', '2018/12/logo.png'),
(107, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:259;s:6:\"height\";i:147;s:4:\"file\";s:16:\"2018/12/logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x147.png\";s:5:\"width\";i:150;s:6:\"height\";i:147;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(108, 58, '_edit_last', '1'),
(123, 65, '_edit_lock', '1545145218:1'),
(124, 66, '_edit_lock', '1545145331:1'),
(125, 67, '_edit_lock', '1545145505:1'),
(122, 64, '_edit_lock', '1545145212:1'),
(113, 54, '_edit_last', '1'),
(114, 52, '_edit_last', '1'),
(115, 50, '_edit_last', '1'),
(116, 48, '_edit_last', '1'),
(117, 46, '_edit_last', '1'),
(149, 78, '_edit_last', '1'),
(127, 69, '_wp_attached_file', '2018/12/5asec.png'),
(128, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:206;s:6:\"height\";i:65;s:4:\"file\";s:17:\"2018/12/5asec.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"5asec-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(129, 70, '_wp_attached_file', '2018/12/abe.png'),
(130, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:186;s:6:\"height\";i:118;s:4:\"file\";s:15:\"2018/12/abe.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"abe-150x118.png\";s:5:\"width\";i:150;s:6:\"height\";i:118;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(131, 71, '_wp_attached_file', '2018/12/adcos.png'),
(132, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:132;s:6:\"height\";i:112;s:4:\"file\";s:17:\"2018/12/adcos.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(133, 72, '_wp_attached_file', '2018/12/boticario.png'),
(134, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:161;s:6:\"height\";i:102;s:4:\"file\";s:21:\"2018/12/boticario.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"boticario-150x102.png\";s:5:\"width\";i:150;s:6:\"height\";i:102;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(135, 73, '_wp_attached_file', '2018/12/globo.png'),
(136, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:118;s:6:\"height\";i:100;s:4:\"file\";s:17:\"2018/12/globo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 74, '_wp_attached_file', '2018/12/google.png'),
(138, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:188;s:6:\"height\";i:61;s:4:\"file\";s:18:\"2018/12/google.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"google-150x61.png\";s:5:\"width\";i:150;s:6:\"height\";i:61;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(139, 75, '_wp_attached_file', '2018/12/hello-1.png'),
(140, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/hello-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 76, '_wp_attached_file', '2018/12/herbarium.png'),
(142, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:224;s:6:\"height\";i:57;s:4:\"file\";s:21:\"2018/12/herbarium.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"herbarium-150x57.png\";s:5:\"width\";i:150;s:6:\"height\";i:57;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(143, 77, '_wp_attached_file', '2018/12/hinode.png'),
(144, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:227;s:6:\"height\";i:105;s:4:\"file\";s:18:\"2018/12/hinode.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"hinode-150x105.png\";s:5:\"width\";i:150;s:6:\"height\";i:105;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(151, 78, 'Ideo_link_cliente', '#'),
(150, 78, '_thumbnail_id', '122'),
(148, 78, '_edit_lock', '1545230646:1'),
(152, 78, '_wp_old_slug', 'principais-clientes'),
(153, 80, '_wp_attached_file', '2018/12/rumo.png'),
(154, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:202;s:6:\"height\";i:46;s:4:\"file\";s:16:\"2018/12/rumo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"rumo-150x46.png\";s:5:\"width\";i:150;s:6:\"height\";i:46;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(155, 81, '_edit_last', '1'),
(156, 81, '_edit_lock', '1545230673:1'),
(157, 82, '_wp_attached_file', '2018/12/jmalucelli.png'),
(158, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:199;s:6:\"height\";i:58;s:4:\"file\";s:22:\"2018/12/jmalucelli.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"jmalucelli-150x58.png\";s:5:\"width\";i:150;s:6:\"height\";i:58;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(159, 81, '_thumbnail_id', '123'),
(160, 81, 'Ideo_link_cliente', '#'),
(161, 83, '_edit_last', '1'),
(162, 83, '_edit_lock', '1545230692:1'),
(163, 83, '_thumbnail_id', '124'),
(164, 83, 'Ideo_link_cliente', '#'),
(165, 84, '_edit_last', '1'),
(166, 84, '_edit_lock', '1545230705:1'),
(167, 84, 'Ideo_link_cliente', '#'),
(168, 84, '_thumbnail_id', '125'),
(169, 85, '_edit_last', '1'),
(170, 85, '_edit_lock', '1545230723:1'),
(171, 85, 'Ideo_link_cliente', '#'),
(172, 86, '_wp_attached_file', '2018/12/merco.png'),
(173, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:163;s:6:\"height\";i:124;s:4:\"file\";s:17:\"2018/12/merco.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"merco-150x124.png\";s:5:\"width\";i:150;s:6:\"height\";i:124;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(174, 85, '_thumbnail_id', '126'),
(175, 87, '_edit_last', '1'),
(176, 87, '_edit_lock', '1545230898:1'),
(177, 87, '_thumbnail_id', '134'),
(178, 87, 'Ideo_link_cliente', '#'),
(179, 88, '_edit_last', '1'),
(180, 88, '_edit_lock', '1545230768:1'),
(181, 89, '_wp_attached_file', '2018/12/ibema.png'),
(182, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:198;s:6:\"height\";i:77;s:4:\"file\";s:17:\"2018/12/ibema.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"ibema-150x77.png\";s:5:\"width\";i:150;s:6:\"height\";i:77;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(183, 88, '_thumbnail_id', '129'),
(184, 88, 'Ideo_link_cliente', '#'),
(185, 90, '_edit_last', '1'),
(186, 90, '_edit_lock', '1545230779:1'),
(187, 90, 'Ideo_link_cliente', '#'),
(188, 90, '_thumbnail_id', '128'),
(189, 91, '_edit_lock', '1545147846:1'),
(190, 94, '_edit_last', '1'),
(191, 94, '_edit_lock', '1545231160:1'),
(192, 95, '_wp_attached_file', '2018/12/tribunal.png'),
(193, 95, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:171;s:6:\"height\";i:99;s:4:\"file\";s:20:\"2018/12/tribunal.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"tribunal-150x99.png\";s:5:\"width\";i:150;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(194, 94, '_thumbnail_id', '141'),
(195, 94, 'Ideo_link_parceiro', '#'),
(196, 96, '_edit_last', '1'),
(197, 96, '_edit_lock', '1545231005:1'),
(198, 96, '_thumbnail_id', '140'),
(199, 96, 'Ideo_link_parceiro', '#'),
(200, 97, '_edit_last', '1'),
(201, 97, '_edit_lock', '1545230991:1'),
(202, 98, '_wp_attached_file', '2018/12/vale.png'),
(203, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:78;s:4:\"file\";s:16:\"2018/12/vale.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"vale-150x78.png\";s:5:\"width\";i:150;s:6:\"height\";i:78;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(204, 97, '_thumbnail_id', '139'),
(205, 97, 'Ideo_link_parceiro', '#'),
(206, 99, '_edit_last', '1'),
(207, 99, '_edit_lock', '1545230966:1'),
(208, 99, '_thumbnail_id', '75'),
(209, 99, 'Ideo_link_parceiro', '#'),
(210, 100, '_edit_last', '1'),
(211, 100, '_edit_lock', '1545230951:1'),
(212, 101, '_wp_attached_file', '2018/12/ibope.png'),
(213, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:50;s:4:\"file\";s:17:\"2018/12/ibope.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"ibope-150x50.png\";s:5:\"width\";i:150;s:6:\"height\";i:50;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(214, 100, '_thumbnail_id', '138'),
(215, 100, 'Ideo_link_parceiro', '#'),
(216, 102, '_edit_last', '1'),
(217, 102, '_edit_lock', '1545230938:1'),
(218, 102, 'Ideo_link_parceiro', '#'),
(219, 103, '_edit_last', '1'),
(220, 103, '_edit_lock', '1545230922:1'),
(221, 103, 'Ideo_link_parceiro', '#'),
(222, 104, '_wp_attached_file', '2018/12/oxford.png'),
(223, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:97;s:6:\"height\";i:105;s:4:\"file\";s:18:\"2018/12/oxford.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(224, 103, '_thumbnail_id', '136'),
(225, 105, '_edit_last', '1'),
(226, 105, '_edit_lock', '1545230910:1'),
(227, 106, '_wp_attached_file', '2018/12/uber.png'),
(228, 106, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:57;s:4:\"file\";s:16:\"2018/12/uber.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"uber-150x57.png\";s:5:\"width\";i:150;s:6:\"height\";i:57;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 105, '_thumbnail_id', '135'),
(230, 105, 'Ideo_link_parceiro', '#'),
(231, 102, '_thumbnail_id', '137'),
(232, 107, '_edit_last', '1'),
(233, 107, '_edit_lock', '1545230791:1'),
(234, 108, '_wp_attached_file', '2018/12/hc.png'),
(235, 108, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2996;s:6:\"height\";i:440;s:4:\"file\";s:14:\"2018/12/hc.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"hc-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"hc-300x44.png\";s:5:\"width\";i:300;s:6:\"height\";i:44;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"hc-768x113.png\";s:5:\"width\";i:768;s:6:\"height\";i:113;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"hc-1024x150.png\";s:5:\"width\";i:1024;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 107, '_thumbnail_id', '130'),
(237, 107, 'Ideo_link_cliente', '#'),
(238, 109, '_edit_last', '1'),
(239, 109, '_edit_lock', '1545230809:1'),
(240, 110, '_wp_attached_file', '2018/12/youtube.png'),
(241, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:5000;s:6:\"height\";i:2085;s:4:\"file\";s:19:\"2018/12/youtube.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"youtube-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"youtube-300x125.png\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"youtube-768x320.png\";s:5:\"width\";i:768;s:6:\"height\";i:320;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"youtube-1024x427.png\";s:5:\"width\";i:1024;s:6:\"height\";i:427;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(242, 109, '_thumbnail_id', '131'),
(243, 109, 'Ideo_link_cliente', '#'),
(244, 111, '_edit_last', '1'),
(245, 111, '_edit_lock', '1545230832:1'),
(246, 111, '_thumbnail_id', '132'),
(247, 111, 'Ideo_link_cliente', '#'),
(248, 112, '_edit_last', '1'),
(249, 112, '_edit_lock', '1545230859:1'),
(250, 112, '_thumbnail_id', '133'),
(251, 112, 'Ideo_link_cliente', '#'),
(252, 113, '_edit_last', '1'),
(253, 113, '_edit_lock', '1545231032:1'),
(254, 114, '_wp_attached_file', '2018/12/TCS.png'),
(255, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:199;s:6:\"height\";i:79;s:4:\"file\";s:15:\"2018/12/TCS.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"TCS-150x79.png\";s:5:\"width\";i:150;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(256, 113, '_thumbnail_id', '142'),
(257, 113, 'Ideo_link_parceiro', '#'),
(258, 115, '_edit_last', '1'),
(259, 115, '_edit_lock', '1545231053:1'),
(260, 116, '_wp_attached_file', '2018/12/michigan.png'),
(261, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:198;s:6:\"height\";i:38;s:4:\"file\";s:20:\"2018/12/michigan.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"michigan-150x38.png\";s:5:\"width\";i:150;s:6:\"height\";i:38;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(262, 115, '_thumbnail_id', '143'),
(263, 115, 'Ideo_link_parceiro', '#'),
(264, 117, '_edit_last', '1'),
(265, 117, '_edit_lock', '1545231086:1'),
(266, 118, '_wp_attached_file', '2018/12/trench.png'),
(267, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:82;s:4:\"file\";s:18:\"2018/12/trench.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"trench-150x82.png\";s:5:\"width\";i:150;s:6:\"height\";i:82;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(268, 117, '_thumbnail_id', '144'),
(269, 117, 'Ideo_link_parceiro', '#'),
(270, 120, '_edit_last', '1'),
(271, 120, '_edit_lock', '1545233768:1'),
(272, 121, '_wp_attached_file', '2018/12/paodeacucar.png'),
(273, 121, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:83;s:4:\"file\";s:23:\"2018/12/paodeacucar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(274, 120, '_thumbnail_id', '145'),
(275, 120, 'Ideo_link_parceiro', '#'),
(276, 122, '_wp_attached_file', '2018/12/rumo-1.png'),
(277, 122, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:18:\"2018/12/rumo-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(278, 123, '_wp_attached_file', '2018/12/jmalucelli-1.png'),
(279, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:24:\"2018/12/jmalucelli-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(280, 124, '_wp_attached_file', '2018/12/herbarium-1.png'),
(281, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:23:\"2018/12/herbarium-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(282, 125, '_wp_attached_file', '2018/12/hinode-1.png'),
(283, 125, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:20:\"2018/12/hinode-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(284, 126, '_wp_attached_file', '2018/12/merco-1.png'),
(285, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/merco-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(286, 127, '_wp_attached_file', '2018/12/5asec-1.png'),
(287, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/5asec-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(288, 128, '_wp_attached_file', '2018/12/adcos-1.png'),
(289, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/adcos-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(290, 129, '_wp_attached_file', '2018/12/ibema-1.png'),
(291, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/ibema-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(292, 130, '_wp_attached_file', '2018/12/hc-1.png'),
(293, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:16:\"2018/12/hc-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(294, 131, '_wp_attached_file', '2018/12/youtube-1.png'),
(295, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:21:\"2018/12/youtube-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(296, 132, '_wp_attached_file', '2018/12/google-1.png'),
(297, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:20:\"2018/12/google-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(298, 133, '_wp_attached_file', '2018/12/boticario-1.png'),
(299, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:23:\"2018/12/boticario-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(300, 134, '_wp_attached_file', '2018/12/5asec-2.png'),
(301, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/5asec-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(302, 135, '_wp_attached_file', '2018/12/uber-1.png'),
(303, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:18:\"2018/12/uber-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(304, 136, '_wp_attached_file', '2018/12/oxford-1.png'),
(305, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:20:\"2018/12/oxford-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(306, 137, '_wp_attached_file', '2018/12/globo-1.png'),
(307, 137, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/globo-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(308, 138, '_wp_attached_file', '2018/12/ibope-1.png'),
(309, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:19:\"2018/12/ibope-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(310, 139, '_wp_attached_file', '2018/12/vale-1.png'),
(311, 139, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:18:\"2018/12/vale-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(312, 140, '_wp_attached_file', '2018/12/abe-1.png'),
(313, 140, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:17:\"2018/12/abe-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(314, 141, '_wp_attached_file', '2018/12/tribunal-1.png'),
(315, 141, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:22:\"2018/12/tribunal-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(316, 142, '_wp_attached_file', '2018/12/TCS-1.png'),
(317, 142, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:17:\"2018/12/TCS-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(318, 143, '_wp_attached_file', '2018/12/michigan-1.png'),
(319, 143, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:22:\"2018/12/michigan-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(320, 144, '_wp_attached_file', '2018/12/trench-1.png'),
(321, 144, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:20:\"2018/12/trench-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(322, 145, '_wp_attached_file', '2018/12/paodeacucar-1.png'),
(323, 145, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:147;s:4:\"file\";s:25:\"2018/12/paodeacucar-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_posts`
--

DROP TABLE IF EXISTS `id_posts`;
CREATE TABLE IF NOT EXISTS `id_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_posts`
--

INSERT INTO `id_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-12-17 10:36:50', '2018-12-17 12:36:50', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2018-12-17 10:36:50', '2018-12-17 12:36:50', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=1', 0, 'post', '', 1),
(2, 1, '2018-12-17 10:36:50', '2018-12-17 12:36:50', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/ideogestaotributaria_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2018-12-17 10:36:50', '2018-12-17 12:36:50', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-12-17 10:36:50', '2018-12-17 12:36:50', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/ideogestaotributaria_site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2018-12-17 10:36:50', '2018-12-17 12:36:50', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-12-17 10:37:11', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-12-17 10:37:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=4', 0, 'post', '', 0),
(5, 1, '2018-12-17 10:37:47', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-12-17 10:37:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=5', 0, 'post', '', 0),
(6, 1, '2018-12-17 11:55:41', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 11:55:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?page_id=6', 0, 'page', '', 0),
(7, 1, '2018-12-17 12:01:01', '2018-12-17 14:01:01', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2018-12-17 15:38:08', '2018-12-17 17:38:08', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?page_id=7', 0, 'page', '', 0),
(8, 1, '2018-12-17 12:01:01', '2018-12-17 14:01:01', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 12:01:01', '2018-12-17 14:01:01', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/2018/12/17/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2018-12-17 12:18:14', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-12-17 12:18:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=9', 0, 'post', '', 0),
(11, 1, '2018-12-17 12:35:32', '2018-12-17 14:35:32', '', 'logo-principal', '', 'inherit', 'open', 'closed', '', 'logo-principal', '', '', '2018-12-17 12:35:32', '2018-12-17 14:35:32', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo-principal.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2018-12-17 14:55:03', '2018-12-17 16:55:03', '', 'back-logo-principal', '', 'inherit', 'open', 'closed', '', 'back-logo-principal', '', '', '2018-12-17 14:55:03', '2018-12-17 16:55:03', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png', 0, 'attachment', 'image/png', 0),
(12, 1, '2018-12-17 14:03:23', '2018-12-17 16:03:23', '<!-- wp:heading -->\n<h2></h2>\n<!-- /wp:heading -->', 'Por que a ideo?', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:03:23', '2018-12-17 16:03:23', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2018-12-17 14:29:16', '2018-12-17 16:29:16', '<!-- wp:paragraph -->\n<p>A IDEO, tem por objetivo fornecer soluções através de lorem ipsum lorem ipsun lorem ipsm lorem ipsum.</p>\n<!-- /wp:paragraph -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:29:16', '2018-12-17 16:29:16', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-12-17 14:31:55', '2018-12-17 16:31:55', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:31:55', '2018-12-17 16:31:55', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-12-17 14:11:18', '2018-12-17 16:11:18', '<!-- wp:paragraph -->\n<p>A IDEO, tem por objetivo fornecer soluções através de lorem ipsum lorem ipsun lorem ipsm lorem ipsum.</p>\n<!-- /wp:paragraph -->', 'Por que a ideo?', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:11:18', '2018-12-17 16:11:18', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-12-17 14:08:20', '2018-12-17 16:08:20', '<!-- wp:paragraph -->\n<p>A IDEO, tem por objetivo fornecer soluções através de lorem ipsum lorem ipsun lorem ipsm lorem ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>E também lorem ipsum lorem ipsum lorem ipsum ipsum ipsum.</p>\n<!-- /wp:paragraph -->', 'Por que a ideo?', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:08:20', '2018-12-17 16:08:20', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-12-17 14:36:08', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:36:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=19', 0, 'destaque', '', 0),
(20, 1, '2018-12-17 14:36:30', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:36:30', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=20', 0, 'destaque', '', 0),
(21, 1, '2018-12-17 14:38:50', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:38:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=21', 0, 'destaque', '', 0),
(22, 1, '2018-12-17 14:39:29', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:39:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=22', 0, 'destaque', '', 0),
(23, 1, '2018-12-17 14:40:25', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:40:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=23', 0, 'destaque', '', 0),
(24, 1, '2018-12-17 14:41:01', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-17 14:41:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=24', 0, 'destaque', '', 0),
(50, 1, '2018-12-18 10:55:56', '2018-12-18 12:55:56', '<!-- wp:paragraph -->\n<p>Análise dos procedmentos fiscais lorem ipsum lorem ipsum lorem ipsum</p>\n<!-- /wp:paragraph -->', 'planejamento tributário', '', 'publish', 'closed', 'closed', '', 'planejamento-tributario', '', '', '2018-12-18 12:03:53', '2018-12-18 14:03:53', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=servicos&#038;p=50', 0, 'servicos', '', 0),
(51, 1, '2018-12-18 10:55:51', '2018-12-18 12:55:51', '', 'servico1', '', 'inherit', 'open', 'closed', '', 'servico1', '', '', '2018-12-18 10:55:51', '2018-12-18 12:55:51', '', 50, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/servico1.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2018-12-18 10:56:41', '2018-12-18 12:56:41', '<!-- wp:paragraph -->\n<p>\n\nRevisoes das apurações de lorem ipsum lorem ipsum lorem ipsum\n\n</p>\n<!-- /wp:paragraph -->', 'revisoes tributarias', '', 'publish', 'closed', 'closed', '', 'revisoes-tributarias', '', '', '2018-12-18 12:02:31', '2018-12-18 14:02:31', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=servicos&#038;p=52', 1, 'servicos', '', 0),
(53, 1, '2018-12-18 10:56:36', '2018-12-18 12:56:36', '', 'servico2', '', 'inherit', 'open', 'closed', '', 'servico2', '', '', '2018-12-18 10:56:36', '2018-12-18 12:56:36', '', 52, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/servico2.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2018-12-17 14:55:23', '2018-12-17 16:55:23', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:55:23', '2018-12-17 16:55:23', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2018-12-18 10:57:24', '2018-12-18 12:57:24', '', 'servico3', '', 'inherit', 'open', 'closed', '', 'servico3', '', '', '2018-12-18 10:57:24', '2018-12-18 12:57:24', '', 54, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/servico3.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2018-12-18 12:02:11', '2018-12-18 14:02:11', '<!-- wp:paragraph -->\n<p>Atuamos na identificação e compatibilização das lorem ipsum lorem ipsum lorem ipsum</p>\n<!-- /wp:paragraph -->', 'incentivos em p&d lei do bem', '', 'publish', 'closed', 'closed', '', 'incentivos-em-pd-lei-do-bem', '', '', '2018-12-18 12:02:11', '2018-12-18 14:02:11', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=servicos&#038;p=56', 3, 'servicos', '', 0),
(57, 1, '2018-12-18 10:58:30', '2018-12-18 12:58:30', '', 'servico4', '', 'inherit', 'open', 'closed', '', 'servico4', '', '', '2018-12-18 10:58:30', '2018-12-18 12:58:30', '', 56, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/servico4.png', 0, 'attachment', 'image/png', 0),
(28, 1, '2018-12-17 14:56:18', '2018-12-17 16:56:18', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>jhigiigy</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>ghhgf</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>gfhfhgfhg</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:56:18', '2018-12-17 16:56:18', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2018-12-18 10:32:25', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-12-18 10:32:25', '2018-12-18 12:32:25', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&#038;p=44', 0, 'destaque', '', 0),
(45, 1, '2018-12-18 10:39:37', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 10:39:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=quem_somos&p=45', 0, 'quem_somos', '', 0),
(46, 1, '2018-12-18 10:52:26', '2018-12-18 12:52:26', '<!-- wp:paragraph -->\n<p>Consultor empresarial, lorem ispu lorem ipsum lorm ipsum<br></p>\n<!-- /wp:paragraph -->', 'fábio morais', '', 'publish', 'closed', 'closed', '', 'fabio-morais', '', '', '2018-12-18 12:05:13', '2018-12-18 14:05:13', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=quem_somos&#038;p=46', 0, 'quem_somos', '', 0),
(47, 1, '2018-12-18 10:51:56', '2018-12-18 12:51:56', '', 'profissional1', '', 'inherit', 'open', 'closed', '', 'profissional1', '', '', '2018-12-18 10:51:56', '2018-12-18 12:51:56', '', 46, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/profissional1.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2018-12-18 10:54:31', '2018-12-18 12:54:31', '<!-- wp:paragraph -->\n<p>Consultor empresarial, lorem ispu lorem ipsum lorm ipsum<br></p>\n<!-- /wp:paragraph -->', 'silvio montenegro', '', 'publish', 'closed', 'closed', '', 'silvio-montenegro', '', '', '2018-12-18 12:05:08', '2018-12-18 14:05:08', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=quem_somos&#038;p=48', 1, 'quem_somos', '', 0),
(49, 1, '2018-12-18 10:54:25', '2018-12-18 12:54:25', '', 'profissional2', '', 'inherit', 'open', 'closed', '', 'profissional2', '', '', '2018-12-18 10:54:25', '2018-12-18 12:54:25', '', 48, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/profissional2.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2018-12-17 14:58:05', '2018-12-17 16:58:05', '', 'hello', '', 'inherit', 'open', 'closed', '', 'hello', '', '', '2018-12-17 14:58:05', '2018-12-17 16:58:05', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2018-12-17 15:28:22', '2018-12-17 17:28:22', '', 'Quem somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=36', 2, 'nav_menu_item', '', 0),
(37, 1, '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 'Servicos', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=37', 3, 'nav_menu_item', '', 0),
(38, 1, '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 'Clientes', '', 'publish', 'closed', 'closed', '', 'clientes', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=38', 4, 'nav_menu_item', '', 0),
(39, 1, '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 'Parceiros', '', 'publish', 'closed', 'closed', '', 'parceiros', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=39', 5, 'nav_menu_item', '', 0),
(40, 1, '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=40', 6, 'nav_menu_item', '', 0),
(54, 1, '2018-12-18 10:57:28', '2018-12-18 12:57:28', '<!-- wp:paragraph -->\n<p>Com os canais estbelecidos nos estados lorem ipsum lorem ipsum lorem ipsum</p>\n<!-- /wp:paragraph -->', 'regimes especiais e incentivos fiscais', '', 'publish', 'closed', 'closed', '', 'regimes-especiais-e-incentivos-fiscais', '', '', '2018-12-18 12:02:26', '2018-12-18 14:02:26', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=servicos&#038;p=54', 2, 'servicos', '', 0),
(30, 1, '2018-12-17 14:58:11', '2018-12-17 16:58:11', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>jhigiigy</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>ghhgf</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>gfhfhgfhg</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:spacer {\"height\":342} -->\n<div style=\"height:342px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:media-text {\"mediaId\":29,\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello.png\" alt=\"\" class=\"wp-image-29\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">alguma coisa</p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:58:11', '2018-12-17 16:58:11', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2018-12-17 14:58:36', '2018-12-17 16:58:36', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>jhigiigy</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>ghhgf</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>gfhfhgfhg</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:spacer {\"height\":342} -->\n<div style=\"height:342px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:media-text {\"mediaId\":29,\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello.png\" alt=\"\" class=\"wp-image-29\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">alguma coisa</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:button -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link\" href=\"pkjpojpoijiopk\"></a></div>\n<!-- /wp:button --></div></div>\n<!-- /wp:media-text -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:58:36', '2018-12-17 16:58:36', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2018-12-17 15:17:23', '2018-12-17 17:17:23', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-12-17 15:29:39', '2018-12-17 17:29:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?p=35', 1, 'nav_menu_item', '', 0),
(32, 1, '2018-12-17 14:58:52', '2018-12-17 16:58:52', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>jhigiigy</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>ghhgf</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>gfhfhgfhg</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:spacer {\"height\":342} -->\n<div style=\"height:342px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:media-text {\"mediaId\":29,\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello.png\" alt=\"\" class=\"wp-image-29\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">alguma coisa</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:button -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link\" href=\"pkjpojpoijiopk\">lkpk</a></div>\n<!-- /wp:button --></div></div>\n<!-- /wp:media-text -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:58:52', '2018-12-17 16:58:52', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2018-12-17 15:00:41', '2018-12-17 17:00:41', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 15:00:41', '2018-12-17 17:00:41', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2018-12-17 14:59:14', '2018-12-17 16:59:14', '<!-- wp:cover {\"url\":\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png\",\"id\":25} -->\n<div class=\"wp-block-cover has-background-dim\" style=\"background-image:url(http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-logo-principal.png)\"><p class=\"wp-block-cover-text\">Maria</p></div>\n<!-- /wp:cover -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>jhigiigy</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>ghhgf</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p>gfhfhgfhg</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns has-2-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:spacer {\"height\":342} -->\n<div style=\"height:342px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:media-text {\"mediaId\":29,\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello.png\" alt=\"\" class=\"wp-image-29\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">alguma coisa</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:button {\"textColor\":\"very-light-gray\",\"customBackgroundColor\":\"#83dc75\",\"className\":\"is-style-outline\"} -->\n<div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link has-text-color has-very-light-gray-color has-background\" href=\"pkjpojpoijiopk\" style=\"background-color:#83dc75\">lkpk</a></div>\n<!-- /wp:button --></div></div>\n<!-- /wp:media-text -->', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-12-17 14:59:14', '2018-12-17 16:59:14', '', 7, 'http://localhost/projetos/ideogestaotributaria_site/7-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-12-18 09:59:43', '2018-12-18 11:59:43', '', 'back-footer', '', 'inherit', 'open', 'closed', '', 'back-footer', '', '', '2018-12-18 09:59:43', '2018-12-18 11:59:43', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/back-footer.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2018-12-18 10:26:01', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 10:26:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=42', 0, 'destaque', '', 0),
(43, 1, '2018-12-18 10:26:01', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 10:26:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=destaque&p=43', 0, 'destaque', '', 0),
(58, 1, '2018-12-18 10:59:19', '2018-12-18 12:59:19', '<!-- wp:paragraph -->\n<p>Suporte na prestação de esclarecimentos lorem ipsum lorem ipsum lorem ipsum</p>\n<!-- /wp:paragraph -->', 'procedimento de fiscalização', '', 'publish', 'closed', 'closed', '', 'procedimento-de-fiscalizacao', '', '', '2018-12-18 12:01:33', '2018-12-18 14:01:33', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=servicos&#038;p=58', 4, 'servicos', '', 0),
(59, 1, '2018-12-18 11:22:07', '2018-12-18 13:22:07', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-12-18 11:22:07', '2018-12-18 13:22:07', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/logo.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2018-12-18 12:55:26', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 12:55:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=62', 0, 'clientes', '', 0),
(63, 1, '2018-12-18 12:56:03', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 12:56:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=63', 0, 'clientes', '', 0),
(64, 1, '2018-12-18 12:56:23', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 12:56:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=64', 0, 'clientes', '', 0),
(65, 1, '2018-12-18 13:02:39', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:02:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=65', 0, 'clientes', '', 0),
(66, 1, '2018-12-18 13:04:33', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:04:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=66', 0, 'clientes', '', 0),
(67, 1, '2018-12-18 13:04:37', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:04:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=67', 0, 'clientes', '', 0),
(90, 1, '2018-12-18 13:33:55', '2018-12-18 15:33:55', '', 'adcos', '', 'publish', 'closed', 'closed', '', 'adcos', '', '', '2018-12-19 12:48:13', '2018-12-19 14:48:13', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=90', 7, 'clientes', '', 0),
(69, 1, '2018-12-18 13:08:14', '2018-12-18 15:08:14', '', '5asec', '', 'inherit', 'open', 'closed', '', '5asec', '', '', '2018-12-18 13:08:14', '2018-12-18 15:08:14', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/5asec.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2018-12-18 13:08:15', '2018-12-18 15:08:15', '', 'abe', '', 'inherit', 'open', 'closed', '', 'abe', '', '', '2018-12-18 13:08:15', '2018-12-18 15:08:15', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/abe.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2018-12-18 13:08:16', '2018-12-18 15:08:16', '', 'adcos', '', 'inherit', 'open', 'closed', '', 'adcos', '', '', '2018-12-18 13:08:16', '2018-12-18 15:08:16', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/adcos.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2018-12-18 13:08:17', '2018-12-18 15:08:17', '', 'boticario', '', 'inherit', 'open', 'closed', '', 'boticario', '', '', '2018-12-18 13:08:17', '2018-12-18 15:08:17', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/boticario.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2018-12-18 13:08:18', '2018-12-18 15:08:18', '', 'globo', '', 'inherit', 'open', 'closed', '', 'globo', '', '', '2018-12-18 13:08:18', '2018-12-18 15:08:18', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/globo.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2018-12-18 13:08:19', '2018-12-18 15:08:19', '', 'google', '', 'inherit', 'open', 'closed', '', 'google', '', '', '2018-12-18 13:08:19', '2018-12-18 15:08:19', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/google.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2018-12-18 13:08:20', '2018-12-18 15:08:20', '', 'hello', '', 'inherit', 'open', 'closed', '', 'hello-2', '', '', '2018-12-18 13:08:20', '2018-12-18 15:08:20', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hello-1.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2018-12-18 13:08:20', '2018-12-18 15:08:20', '', 'herbarium', '', 'inherit', 'open', 'closed', '', 'herbarium', '', '', '2018-12-18 13:08:20', '2018-12-18 15:08:20', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/herbarium.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2018-12-18 13:08:21', '2018-12-18 15:08:21', '', 'hinode', '', 'inherit', 'open', 'closed', '', 'hinode', '', '', '2018-12-18 13:08:21', '2018-12-18 15:08:21', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hinode.png', 0, 'attachment', 'image/png', 0),
(78, 1, '2018-12-18 13:20:18', '2018-12-18 15:20:18', '<!-- wp:paragraph -->\n<p>Nossos clientes atuam no desenvolvimento, fabricação, distribuição e comercialização de produtos, bem como na prestação de serviços.</p>\n<!-- /wp:paragraph -->', 'rumo', '', 'publish', 'closed', 'closed', '', 'rumo', '', '', '2018-12-19 12:46:21', '2018-12-19 14:46:21', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=78', 0, 'clientes', '', 0),
(79, 1, '2018-12-18 13:30:08', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:30:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&p=79', 0, 'clientes', '', 0),
(80, 1, '2018-12-18 13:30:48', '2018-12-18 15:30:48', '', 'rumo', '', 'inherit', 'open', 'closed', '', 'rumo-2', '', '', '2018-12-18 13:30:48', '2018-12-18 15:30:48', '', 78, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/rumo.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2018-12-18 13:31:39', '2018-12-18 15:31:39', '', 'jmalucelli', '', 'publish', 'closed', 'closed', '', 'jmalucelli', '', '', '2018-12-19 12:46:47', '2018-12-19 14:46:47', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=81', 1, 'clientes', '', 0),
(82, 1, '2018-12-18 13:31:36', '2018-12-18 15:31:36', '', 'jmalucelli', '', 'inherit', 'open', 'closed', '', 'jmalucelli', '', '', '2018-12-18 13:31:36', '2018-12-18 15:31:36', '', 81, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/jmalucelli.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2018-12-18 13:31:55', '2018-12-18 15:31:55', '', 'herbarium', '', 'publish', 'closed', 'closed', '', 'herbarium', '', '', '2018-12-19 12:47:15', '2018-12-19 14:47:15', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=83', 2, 'clientes', '', 0),
(84, 1, '2018-12-18 13:32:05', '2018-12-18 15:32:05', '', 'hinode', '', 'publish', 'closed', 'closed', '', 'hinode', '', '', '2018-12-19 12:47:27', '2018-12-19 14:47:27', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=84', 3, 'clientes', '', 0),
(85, 1, '2018-12-18 13:32:27', '2018-12-18 15:32:27', '', 'merco', '', 'publish', 'closed', 'closed', '', 'merco', '', '', '2018-12-19 12:47:45', '2018-12-19 14:47:45', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=85', 4, 'clientes', '', 0),
(86, 1, '2018-12-18 13:32:45', '2018-12-18 15:32:45', '', 'merco', '', 'inherit', 'open', 'closed', '', 'merco-2', '', '', '2018-12-18 13:32:45', '2018-12-18 15:32:45', '', 85, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/merco.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2018-12-18 13:33:15', '2018-12-18 15:33:15', '', '5 asec', '', 'publish', 'closed', 'closed', '', '5-asec', '', '', '2018-12-19 12:50:18', '2018-12-19 14:50:18', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=87', 5, 'clientes', '', 0),
(88, 1, '2018-12-18 13:33:45', '2018-12-18 15:33:45', '', 'ibema', '', 'publish', 'closed', 'closed', '', 'ibema', '', '', '2018-12-19 12:48:31', '2018-12-19 14:48:31', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=88', 6, 'clientes', '', 0),
(89, 1, '2018-12-18 13:33:43', '2018-12-18 15:33:43', '', 'ibema', '', 'inherit', 'open', 'closed', '', 'ibema', '', '', '2018-12-18 13:33:43', '2018-12-18 15:33:43', '', 88, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/ibema.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2018-12-18 13:45:44', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:45:44', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&p=91', 0, 'parceiros', '', 0),
(92, 1, '2018-12-18 13:46:33', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:46:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&p=92', 0, 'parceiros', '', 0),
(93, 1, '2018-12-18 13:46:58', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-18 13:46:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&p=93', 0, 'parceiros', '', 0),
(94, 1, '2018-12-18 13:47:57', '2018-12-18 15:47:57', '', 'Tribunal de justiça', '', 'publish', 'closed', 'closed', '', 'tribunal-de-justica', '', '', '2018-12-19 12:52:39', '2018-12-19 14:52:39', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=94', 7, 'parceiros', '', 0),
(95, 1, '2018-12-18 13:47:54', '2018-12-18 15:47:54', '', 'tribunal', '', 'inherit', 'open', 'closed', '', 'tribunal', '', '', '2018-12-18 13:47:54', '2018-12-18 15:47:54', '', 94, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/tribunal.png', 0, 'attachment', 'image/png', 0),
(96, 1, '2018-12-18 13:48:13', '2018-12-18 15:48:13', '', 'abe', '', 'publish', 'closed', 'closed', '', 'abe', '', '', '2018-12-19 12:52:27', '2018-12-19 14:52:27', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=96', 6, 'parceiros', '', 0),
(97, 1, '2018-12-18 13:48:32', '2018-12-18 15:48:32', '', 'vale', '', 'publish', 'closed', 'closed', '', 'vale', '', '', '2018-12-19 12:52:00', '2018-12-19 14:52:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=97', 5, 'parceiros', '', 0),
(98, 1, '2018-12-18 13:48:29', '2018-12-18 15:48:29', '', 'vale', '', 'inherit', 'open', 'closed', '', 'vale', '', '', '2018-12-18 13:48:29', '2018-12-18 15:48:29', '', 97, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/vale.png', 0, 'attachment', 'image/png', 0),
(99, 1, '2018-12-18 13:48:51', '2018-12-18 15:48:51', '', 'hello study', '', 'publish', 'closed', 'closed', '', 'hello-study', '', '', '2018-12-18 13:48:51', '2018-12-18 15:48:51', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=99', 4, 'parceiros', '', 0),
(100, 1, '2018-12-18 13:49:38', '2018-12-18 15:49:38', '', 'ibope', '', 'publish', 'closed', 'closed', '', 'ibope', '', '', '2018-12-19 12:51:32', '2018-12-19 14:51:32', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=100', 3, 'parceiros', '', 0),
(101, 1, '2018-12-18 13:49:35', '2018-12-18 15:49:35', '', 'ibope', '', 'inherit', 'open', 'closed', '', 'ibope', '', '', '2018-12-18 13:49:35', '2018-12-18 15:49:35', '', 100, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/ibope.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2018-12-18 13:49:49', '2018-12-18 15:49:49', '', 'globo', '', 'publish', 'closed', 'closed', '', 'globo', '', '', '2018-12-19 12:51:20', '2018-12-19 14:51:20', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=102', 2, 'parceiros', '', 0),
(103, 1, '2018-12-18 13:50:04', '2018-12-18 15:50:04', '', 'oxford', '', 'publish', 'closed', 'closed', '', 'oxford', '', '', '2018-12-19 12:51:04', '2018-12-19 14:51:04', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=103', 1, 'parceiros', '', 0);
INSERT INTO `id_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(104, 1, '2018-12-18 13:51:30', '2018-12-18 15:51:30', '', 'oxford', '', 'inherit', 'open', 'closed', '', 'oxford-2', '', '', '2018-12-18 13:51:30', '2018-12-18 15:51:30', '', 103, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/oxford.png', 0, 'attachment', 'image/png', 0),
(105, 1, '2018-12-18 13:52:23', '2018-12-18 15:52:23', '', 'uber', '', 'publish', 'closed', 'closed', '', 'uber', '', '', '2018-12-19 12:50:52', '2018-12-19 14:50:52', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=105', 0, 'parceiros', '', 0),
(106, 1, '2018-12-18 13:52:17', '2018-12-18 15:52:17', '', 'uber', '', 'inherit', 'open', 'closed', '', 'uber', '', '', '2018-12-18 13:52:17', '2018-12-18 15:52:17', '', 105, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/uber.png', 0, 'attachment', 'image/png', 0),
(107, 1, '2018-12-19 08:48:37', '2018-12-19 10:48:37', '', 'hc desenvolvimentos', '', 'publish', 'closed', 'closed', '', 'hc-desenvolvimentos', '', '', '2018-12-19 12:48:53', '2018-12-19 14:48:53', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=107', 8, 'clientes', '', 0),
(108, 1, '2018-12-19 08:48:33', '2018-12-19 10:48:33', '', 'hc', '', 'inherit', 'open', 'closed', '', 'hc', '', '', '2018-12-19 08:48:33', '2018-12-19 10:48:33', '', 107, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hc.png', 0, 'attachment', 'image/png', 0),
(109, 1, '2018-12-19 08:51:33', '2018-12-19 10:51:33', '', 'youtube', '', 'publish', 'closed', 'closed', '', 'youtube', '', '', '2018-12-19 12:49:09', '2018-12-19 14:49:09', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=109', 9, 'clientes', '', 0),
(110, 1, '2018-12-19 08:51:28', '2018-12-19 10:51:28', '', 'youtube', '', 'inherit', 'open', 'closed', '', 'youtube', '', '', '2018-12-19 08:51:28', '2018-12-19 10:51:28', '', 109, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/youtube.png', 0, 'attachment', 'image/png', 0),
(111, 1, '2018-12-19 08:51:52', '2018-12-19 10:51:52', '', 'google', '', 'publish', 'closed', 'closed', '', 'google', '', '', '2018-12-19 12:49:33', '2018-12-19 14:49:33', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=111', 10, 'clientes', '', 0),
(112, 1, '2018-12-19 08:52:19', '2018-12-19 10:52:19', '', 'boticario', '', 'publish', 'closed', 'closed', '', 'boticario', '', '', '2018-12-19 12:49:52', '2018-12-19 14:49:52', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=clientes&#038;p=112', 11, 'clientes', '', 0),
(113, 1, '2018-12-19 08:58:28', '2018-12-19 10:58:28', '', 'tcs', '', 'publish', 'closed', 'closed', '', 'tcs', '', '', '2018-12-19 12:52:53', '2018-12-19 14:52:53', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=113', 8, 'parceiros', '', 0),
(114, 1, '2018-12-19 08:58:26', '2018-12-19 10:58:26', '', 'TCS', '', 'inherit', 'open', 'closed', '', 'tcs', '', '', '2018-12-19 08:58:26', '2018-12-19 10:58:26', '', 113, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/TCS.png', 0, 'attachment', 'image/png', 0),
(115, 1, '2018-12-19 08:58:59', '2018-12-19 10:58:59', '', 'university of michigan', '', 'publish', 'closed', 'closed', '', 'university-of-michigan', '', '', '2018-12-19 12:53:14', '2018-12-19 14:53:14', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=115', 9, 'parceiros', '', 0),
(116, 1, '2018-12-19 08:58:56', '2018-12-19 10:58:56', '', 'michigan', '', 'inherit', 'open', 'closed', '', 'michigan', '', '', '2018-12-19 08:58:56', '2018-12-19 10:58:56', '', 115, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/michigan.png', 0, 'attachment', 'image/png', 0),
(117, 1, '2018-12-19 08:59:32', '2018-12-19 10:59:32', '', 'trench', '', 'publish', 'closed', 'closed', '', 'trench', '', '', '2018-12-19 12:53:48', '2018-12-19 14:53:48', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=117', 10, 'parceiros', '', 0),
(118, 1, '2018-12-19 08:59:29', '2018-12-19 10:59:29', '', 'trench', '', 'inherit', 'open', 'closed', '', 'trench', '', '', '2018-12-19 08:59:29', '2018-12-19 10:59:29', '', 117, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/trench.png', 0, 'attachment', 'image/png', 0),
(119, 1, '2018-12-19 08:59:35', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-12-19 08:59:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&p=119', 0, 'parceiros', '', 0),
(120, 1, '2018-12-19 08:59:57', '2018-12-19 10:59:57', '', 'pão de açucar', '', 'publish', 'closed', 'closed', '', 'pao-de-acucar', '', '', '2018-12-19 12:54:08', '2018-12-19 14:54:08', '', 0, 'http://localhost/projetos/ideogestaotributaria_site/?post_type=parceiros&#038;p=120', 11, 'parceiros', '', 0),
(121, 1, '2018-12-19 08:59:55', '2018-12-19 10:59:55', '', 'paodeacucar', '', 'inherit', 'open', 'closed', '', 'paodeacucar', '', '', '2018-12-19 08:59:55', '2018-12-19 10:59:55', '', 120, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/paodeacucar.png', 0, 'attachment', 'image/png', 0),
(122, 1, '2018-12-19 12:46:19', '2018-12-19 14:46:19', '', 'rumo', '', 'inherit', 'open', 'closed', '', 'rumo-3', '', '', '2018-12-19 12:46:19', '2018-12-19 14:46:19', '', 78, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/rumo-1.png', 0, 'attachment', 'image/png', 0),
(123, 1, '2018-12-19 12:46:42', '2018-12-19 14:46:42', '', 'jmalucelli', '', 'inherit', 'open', 'closed', '', 'jmalucelli-2', '', '', '2018-12-19 12:46:42', '2018-12-19 14:46:42', '', 81, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/jmalucelli-1.png', 0, 'attachment', 'image/png', 0),
(124, 1, '2018-12-19 12:47:08', '2018-12-19 14:47:08', '', 'herbarium', '', 'inherit', 'open', 'closed', '', 'herbarium-2', '', '', '2018-12-19 12:47:08', '2018-12-19 14:47:08', '', 83, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/herbarium-1.png', 0, 'attachment', 'image/png', 0),
(125, 1, '2018-12-19 12:47:25', '2018-12-19 14:47:25', '', 'hinode', '', 'inherit', 'open', 'closed', '', 'hinode-2', '', '', '2018-12-19 12:47:25', '2018-12-19 14:47:25', '', 84, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hinode-1.png', 0, 'attachment', 'image/png', 0),
(126, 1, '2018-12-19 12:47:42', '2018-12-19 14:47:42', '', 'merco', '', 'inherit', 'open', 'closed', '', 'merco-3', '', '', '2018-12-19 12:47:42', '2018-12-19 14:47:42', '', 85, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/merco-1.png', 0, 'attachment', 'image/png', 0),
(127, 1, '2018-12-19 12:47:59', '2018-12-19 14:47:59', '', '5asec', '', 'inherit', 'open', 'closed', '', '5asec-2', '', '', '2018-12-19 12:47:59', '2018-12-19 14:47:59', '', 87, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/5asec-1.png', 0, 'attachment', 'image/png', 0),
(128, 1, '2018-12-19 12:48:11', '2018-12-19 14:48:11', '', 'adcos', '', 'inherit', 'open', 'closed', '', 'adcos-2', '', '', '2018-12-19 12:48:11', '2018-12-19 14:48:11', '', 90, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/adcos-1.png', 0, 'attachment', 'image/png', 0),
(129, 1, '2018-12-19 12:48:28', '2018-12-19 14:48:28', '', 'ibema', '', 'inherit', 'open', 'closed', '', 'ibema-2', '', '', '2018-12-19 12:48:28', '2018-12-19 14:48:28', '', 88, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/ibema-1.png', 0, 'attachment', 'image/png', 0),
(130, 1, '2018-12-19 12:48:50', '2018-12-19 14:48:50', '', 'hc', '', 'inherit', 'open', 'closed', '', 'hc-2', '', '', '2018-12-19 12:48:50', '2018-12-19 14:48:50', '', 107, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/hc-1.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2018-12-19 12:49:05', '2018-12-19 14:49:05', '', 'youtube', '', 'inherit', 'open', 'closed', '', 'youtube-2', '', '', '2018-12-19 12:49:05', '2018-12-19 14:49:05', '', 109, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/youtube-1.png', 0, 'attachment', 'image/png', 0),
(132, 1, '2018-12-19 12:49:31', '2018-12-19 14:49:31', '', 'google', '', 'inherit', 'open', 'closed', '', 'google-2', '', '', '2018-12-19 12:49:31', '2018-12-19 14:49:31', '', 111, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/google-1.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2018-12-19 12:49:50', '2018-12-19 14:49:50', '', 'boticario', '', 'inherit', 'open', 'closed', '', 'boticario-2', '', '', '2018-12-19 12:49:50', '2018-12-19 14:49:50', '', 112, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/boticario-1.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2018-12-19 12:50:14', '2018-12-19 14:50:14', '', '5asec', '', 'inherit', 'open', 'closed', '', '5asec-3', '', '', '2018-12-19 12:50:14', '2018-12-19 14:50:14', '', 87, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/5asec-2.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2018-12-19 12:50:50', '2018-12-19 14:50:50', '', 'uber', '', 'inherit', 'open', 'closed', '', 'uber-2', '', '', '2018-12-19 12:50:50', '2018-12-19 14:50:50', '', 105, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/uber-1.png', 0, 'attachment', 'image/png', 0),
(136, 1, '2018-12-19 12:51:02', '2018-12-19 14:51:02', '', 'oxford', '', 'inherit', 'open', 'closed', '', 'oxford-3', '', '', '2018-12-19 12:51:02', '2018-12-19 14:51:02', '', 103, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/oxford-1.png', 0, 'attachment', 'image/png', 0),
(137, 1, '2018-12-19 12:51:15', '2018-12-19 14:51:15', '', 'globo', '', 'inherit', 'open', 'closed', '', 'globo-2', '', '', '2018-12-19 12:51:15', '2018-12-19 14:51:15', '', 102, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/globo-1.png', 0, 'attachment', 'image/png', 0),
(138, 1, '2018-12-19 12:51:30', '2018-12-19 14:51:30', '', 'ibope', '', 'inherit', 'open', 'closed', '', 'ibope-2', '', '', '2018-12-19 12:51:30', '2018-12-19 14:51:30', '', 100, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/ibope-1.png', 0, 'attachment', 'image/png', 0),
(139, 1, '2018-12-19 12:51:57', '2018-12-19 14:51:57', '', 'vale', '', 'inherit', 'open', 'closed', '', 'vale-2', '', '', '2018-12-19 12:51:57', '2018-12-19 14:51:57', '', 97, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/vale-1.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2018-12-19 12:52:24', '2018-12-19 14:52:24', '', 'abe', '', 'inherit', 'open', 'closed', '', 'abe-2', '', '', '2018-12-19 12:52:24', '2018-12-19 14:52:24', '', 96, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/abe-1.png', 0, 'attachment', 'image/png', 0),
(141, 1, '2018-12-19 12:52:37', '2018-12-19 14:52:37', '', 'tribunal', '', 'inherit', 'open', 'closed', '', 'tribunal-2', '', '', '2018-12-19 12:52:37', '2018-12-19 14:52:37', '', 94, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/tribunal-1.png', 0, 'attachment', 'image/png', 0),
(142, 1, '2018-12-19 12:52:49', '2018-12-19 14:52:49', '', 'TCS', '', 'inherit', 'open', 'closed', '', 'tcs-2', '', '', '2018-12-19 12:52:49', '2018-12-19 14:52:49', '', 113, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/TCS-1.png', 0, 'attachment', 'image/png', 0),
(143, 1, '2018-12-19 12:53:06', '2018-12-19 14:53:06', '', 'michigan', '', 'inherit', 'open', 'closed', '', 'michigan-2', '', '', '2018-12-19 12:53:06', '2018-12-19 14:53:06', '', 115, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/michigan-1.png', 0, 'attachment', 'image/png', 0),
(144, 1, '2018-12-19 12:53:28', '2018-12-19 14:53:28', '', 'trench', '', 'inherit', 'open', 'closed', '', 'trench-2', '', '', '2018-12-19 12:53:28', '2018-12-19 14:53:28', '', 117, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/trench-1.png', 0, 'attachment', 'image/png', 0),
(145, 1, '2018-12-19 12:54:04', '2018-12-19 14:54:04', '', 'paodeacucar', '', 'inherit', 'open', 'closed', '', 'paodeacucar-2', '', '', '2018-12-19 12:54:04', '2018-12-19 14:54:04', '', 120, 'http://localhost/projetos/ideogestaotributaria_site/wp-content/uploads/2018/12/paodeacucar-1.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_termmeta`
--

DROP TABLE IF EXISTS `id_termmeta`;
CREATE TABLE IF NOT EXISTS `id_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_terms`
--

DROP TABLE IF EXISTS `id_terms`;
CREATE TABLE IF NOT EXISTS `id_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_terms`
--

INSERT INTO `id_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_term_relationships`
--

DROP TABLE IF EXISTS `id_term_relationships`;
CREATE TABLE IF NOT EXISTS `id_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_term_relationships`
--

INSERT INTO `id_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(35, 2, 0),
(36, 2, 0),
(37, 2, 0),
(38, 2, 0),
(39, 2, 0),
(40, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_term_taxonomy`
--

DROP TABLE IF EXISTS `id_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `id_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_term_taxonomy`
--

INSERT INTO `id_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_usermeta`
--

DROP TABLE IF EXISTS `id_usermeta`;
CREATE TABLE IF NOT EXISTS `id_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_usermeta`
--

INSERT INTO `id_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'ideo'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'id_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'id_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(20, 1, 'session_tokens', 'a:2:{s:64:\"77580d0d3a6e1554efba14b0b6ada3dd18af2fc0bfc271241da53a24135f49f9\";a:4:{s:10:\"expiration\";i:1545304157;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36\";s:5:\"login\";i:1545131357;}s:64:\"e57059fb04c85031c9be0cb65f239f0312b02e05a40971446a11fb3852f9c120\";a:4:{s:10:\"expiration\";i:1545389205;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1545216405;}}'),
(17, 1, 'id_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'id_user-settings', 'libraryContent=browse&editor=html'),
(19, 1, 'id_user-settings-time', '1545146760'),
(21, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:30:\"detalhesMetaboxDestaqueInicial\";}'),
(22, 1, 'metaboxhidden_destaque', 'a:0:{}'),
(23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:22:\"add-post-type-destaque\";i:1;s:12:\"add-post_tag\";i:2;s:21:\"add-categoriaDestaque\";}'),
(25, 1, 'nav_menu_recently_edited', '2'),
(26, 1, 'closedpostboxes_clientes', 'a:0:{}'),
(27, 1, 'metaboxhidden_clientes', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `id_users`
--

DROP TABLE IF EXISTS `id_users`;
CREATE TABLE IF NOT EXISTS `id_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `id_users`
--

INSERT INTO `id_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'ideo', '$P$B.Z/qLVjsmghM1/ysTpUN07PZYdsGo1', 'ideo', 'agenciahcdesenvolvimentos@gmail.com', '', '2018-12-17 12:36:50', '', 0, 'ideo');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
