<?php

/**
 * Plugin Name: Base IDEO
 * Description: Controle base do tema IDEO.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: https://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseProjeto () {

		// TIPOS DE CONTEÚDO
		conteudosProjeto();

		taxonomiaProjeto();

		metaboxesProjeto();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosProjeto (){

		// TIPOS DE CONTEÚDO
		tipoDestaque();

		tipoQuemSomos();

		tipoServicos();
		
		tipoClientes();

		tipoParceiros();



		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
			'name'               => 'Destaques',
			'singular_name'      => 'destaque',
			'menu_name'          => 'Destaques',
			'name_admin_bar'     => 'Destaques',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaque 	= array(
			'labels'             => $rotulosDestaque,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-megaphone',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaque' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest'       => true,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}


	function tipoQuemSomos() {

		$rotulosQuemSomos = array(
			'name'               => 'Quem Somos',
			'singular_name'      => 'quem_somos',
			'menu_name'          => 'Quem Somos',
			'name_admin_bar'     => 'Quem Somos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo quem somos',
			'new_item'           => 'Novo quem somos',
			'edit_item'          => 'Editar quem somos',
			'view_item'          => 'Ver quem somos',
			'all_items'          => 'Todos os quem somos',
			'search_items'       => 'Buscar quem somos',
			'parent_item_colon'  => 'Dos quem somos',
			'not_found'          => 'Nenhum quem somos cadastrado.',
			'not_found_in_trash' => 'Nenhum quem somos na lixeira.'
		);

		$argsQuemSomos 	= array(
			'labels'             => $rotulosQuemSomos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 5,
			'menu_icon'          => 'dashicons-groups',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'quem_somos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest'       => true,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('quem_somos', $argsQuemSomos);

	}

	function tipoServicos() {

		$rotulosServicos = array(
			'name'               => 'Serviços',
			'singular_name'      => 'servicos',
			'menu_name'          => 'Serviços',
			'name_admin_bar'     => 'Serviços',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo serviço',
			'new_item'           => 'Novo serviço',
			'edit_item'          => 'Editar serviço',
			'view_item'          => 'Ver serviço',
			'all_items'          => 'Todos os serviços',
			'search_items'       => 'Buscar serviço',
			'parent_item_colon'  => 'Dos serviços',
			'not_found'          => 'Nenhum serviço cadastrado.',
			'not_found_in_trash' => 'Nenhum serviço na lixeira.'
		);

		$argsServicos 	= array(
			'labels'             => $rotulosServicos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 6,
			'menu_icon'          => 'dashicons-hammer',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'servico' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest'       => true,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servicos', $argsServicos);

	}

	function tipoClientes() {

		$rotulosClientes = array(
			'name'               => 'Clientes',
			'singular_name'      => 'clientes',
			'menu_name'          => 'Clientes',
			'name_admin_bar'     => 'Clientes',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo cliente',
			'new_item'           => 'Novo cliente',
			'edit_item'          => 'Editar cliente',
			'view_item'          => 'Ver clientes',
			'all_items'          => 'Todos os clientes',
			'search_items'       => 'Buscar cliente',
			'parent_item_colon'  => 'Dos clientes',
			'not_found'          => 'Nenhum cliente cadastrado.',
			'not_found_in_trash' => 'Nenhum cliente na lixeira.'
		);

		$argsClientes 	= array(
			'labels'             => $rotulosClientes,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 7,
			'menu_icon'          => 'dashicons-smiley',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'clientes' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest'       => true,
			'supports'           => array( 'title', 'thumbnail' )
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('clientes', $argsClientes);

	}

	function tipoParceiros() {

		$rotulosParceiros = array(
			'name'               => 'Parceiros',
			'singular_name'      => 'parceiros',
			'menu_name'          => 'Parceiros',
			'name_admin_bar'     => 'Parceiros',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo parceiro',
			'new_item'           => 'Novo parceiro',
			'edit_item'          => 'Editar parceiro',
			'view_item'          => 'Ver parceiros',
			'all_items'          => 'Todos os parceiros',
			'search_items'       => 'Buscar parceiro',
			'parent_item_colon'  => 'Dos parceiros',
			'not_found'          => 'Nenhum parceiro cadastrado.',
			'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
		);

		$argsParceiros 	= array(
			'labels'             => $rotulosParceiros,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 8,
			'menu_icon'          => 'dashicons-networking',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'parceiro' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest'       => true,
			'supports'           => array( 'title', 'thumbnail' )
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiros', $argsParceiros);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaProjeto () {		
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesProjeto(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Ideo_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesCliente',
				'title'			=> 'Detalhes do cliente',
				'pages' 		=> array( 'clientes' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link cliente: ',
						'id'    => "{$prefix}link_cliente",
						'desc'  => '',
						'type'  => 'text',
					),										
				),
			);

			$metaboxes[] = array(

				'id'			=> 'detalhesParceiros',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'parceiros' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link parceiros: ',
						'id'    => "{$prefix}link_parceiro",
						'desc'  => '',
						'type'  => 'text',
					),										
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesProjeto(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerProjeto(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseProjeto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );