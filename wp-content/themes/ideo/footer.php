<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ideo
 */

global $configuracao;

?>

		<footer>
			<div class="footer" id="contato" style="background-image: url(<?php echo $configuracao['img_fundo_footer']['url'] ?>);">

				<div class="titulo-footer">
					<h5><?php echo $configuracao['titulo_footer'] ?></h5>
				</div>

				<div class="contato">
					<ul class="redes-sociais">
					<?php if($configuracao['redes_sociais_footer_whatsapp']): ?>
						<li class="wpp"><a href="<?php echo $configuracao['redes_sociais_footer_whatsapp'] ?>" title="WhatsApp"><i class="fab fa-whatsapp"></i></a></li>
					<?php endif; if($configuracao['redes_sociais_footer_instagram']): ?>
						<li class="insta"><a href="<?php echo $configuracao['redes_sociais_footer_instagram'] ?>" title="Instagram"><i class="fab fa-instagram"></i></a></li>
					<?php endif; if($configuracao['redes_sociais_footer_facebook']): ?>
						<li class="fb-footer fb" title="Facebook"><a href="<?php echo $configuracao['redes_sociais_footer_facebook']; ?>" class="fb-footer"><i class="fab fa-facebook-f"></i></a></li>
					<?php endif; if($configuracao['redes_sociais_footer_email']): ?>
						<li class="email"><a href="<?php echo $configuracao['redes_sociais_footer_email']; ?>" title="Email"><i class="far fa-envelope"></i></a></li>
					<?php endif; ?>
					</ul>

					<div class="telefone">
						<p>Ou ligue para:</p>
						<p><?php echo $configuracao['telefone_footer'] ?></p>
					</div>

				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<?php 
					$menu = array(
						'theme_location'  => '',
						'menu'            => 'Menu principal',
						'container'       => false,
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu-footer',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 2,
						'walker'          => ''
					);
					wp_nav_menu( $menu );
					?>
					<ul class="redes-copy">
						<?php if($configuracao['redes_sociais_footer_linkedin']): ?>
						<li>
							<a href="<?php echo $configuracao['redes_sociais_footer_linkedin']; ?>" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
						</li>
						<?php endif; if($configuracao['redes_sociais_footer_twitter']): ?>
						<li>
							<a href="<?php echo $configuracao['redes_sociais_footer_twitter']; ?>" title="Twitter"><i class="fab fa-twitter"></i></a>
						</li>
						<?php endif; if($configuracao['redes_sociais_footer_facebook']): ?>
						<li>
							<a href="<?php echo $configuracao['redes_sociais_footer_facebook']; ?>" title="Facebook" class="fb-footer"><i class="fab fa-facebook-f"></i></a>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</footer>
			
		<?php wp_footer(); ?>

	</body>
</html>