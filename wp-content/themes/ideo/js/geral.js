$(function(){

	var mostrarLogoInicial = false;
	var mostrarPorqueAideo = false;
	var mostrarTituloFooter = false;
	var mostrarTelefone = false;
	var mostrarDescClientes = false;
	var mostrarDescParceiros = false;
	// var mostrarServicos = false;

	$('.open-menu').click(function(){
		$('.side-menu-mobile').addClass('open');
	});

	$('.side-menu-mobile span').click(function(){
		$('.side-menu-mobile').removeClass('open');
	});

	var $menu = $('html, body');
	$('.side-menu .menu li a').click(function() {
		$menu.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		return false;
	});

	var $mobile = $('html, body');
	$('.side-menu-mobile .menu li a').click(function() {
		$mobile.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		$('.side-menu-mobile').removeClass('open');

		return false;
	});

	var $copy = $('html, body');
	$('footer .copyright .menu-footer li a').click(function() {
		$copy.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);

		return false;
	});

	var $logo = $('html, body');
	$('.side-menu a').click(function() {
		$logo.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);

		return false;
	});

	$.fn.isOnScreen = function(){
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};

		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};

	$(window).load(function(){
		
		if($('.conteudo .principal .logo-principal').isOnScreen() == true && mostrarLogoInicial == false) {
			mostrarLogoInicial = true;
			$('.conteudo .principal .logo-principal img').addClass('trsLogoInicial');
		}

	});

	$(window).scroll(function(){

		if($('.conteudo .principal .logo-principal').isOnScreen() == true && mostrarLogoInicial == false) {
			mostrarLogoInicial = true;
			$('.conteudo .principal .logo-principal img').addClass('trsLogoInicial');
		}

		if($('.conteudo .porque-aideo').isOnScreen() == true && mostrarPorqueAideo == false) {
			mostrarPorqueAideo = true;
			$('.conteudo .porque-aideo .porque-aideo-cont').addClass('trs');
		}

		if($('footer .footer .titulo-footer').isOnScreen() == true && mostrarTituloFooter == false) {
			mostrarTituloFooter = true;
			$('footer .footer .titulo-footer h5').addClass('trsH5');
		}

		if($('footer .footer .contato').isOnScreen() == true && mostrarTelefone == false) {
			mostrarTelefone = true;
			$('footer .footer .contato .telefone').addClass('trsTelefone');
		}

		if($('.conteudo .clientes .desc').isOnScreen() == true && mostrarDescClientes == false) {
			mostrarDescClientes = true;
			$('.conteudo .clientes .desc h5').addClass('trsDesc');
		}

		if($('.conteudo .parceiros .desc').isOnScreen() == true && mostrarDescParceiros == false) {
			mostrarDescParceiros = true;
			$('.conteudo .parceiros .desc h5').addClass('trsDescP');
		}

		// if($('.conteudo .porque-aideo').isOnScreen() == true && mostra == false) {
		// 	mostra = true;
		// 	$('.conteudo .porque-aideo .porque-aideo-cont').addClass('trs');
		// }

		// if($('.conteudo .servicos ul').isOnScreen() == true && mostrarServicos == false){
		// 	for(i in li){
		// 		setTimeout(function(){
		// 			$(i).addClass('trsLi');
		// 		}, 1000);
		// 	}
		// 	console.log(i);
		// 	mostrarServicos = true;
		// }
	});
});

