<?php
/**
 * Template Name: Inicial

 * @package Ideo_Gestão_Tributária

 */


get_header(); ?>

	<div class="pg-inicial">
		<div class="containerFull">
			<div class="open-menu" title="Abrir menu">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="side-menu-mobile">
				<a href="#home">
					<figure class="logo">
						<img src="<?php echo $configuracao['logo_side_menu']['url'] ?>" alt="Logo">
					</figure>
				</a>
				<span>X</span>
				<?php 
				$menu = array(
					'theme_location'  => '',
					'menu'            => 'Menu principal',
					'container'       => false,
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'menu',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 2,
					'walker'          => ''
				);
				wp_nav_menu( $menu );
				?>
				<div class="social">
				<?php if($configuracao['redes_sociais_footer_facebook']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_facebook'] ?>" class="fb" title="Facebook"><i class="fab fa-facebook-f"></i></a>
				<?php endif; if($configuracao['redes_sociais_footer_linkedin']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_linkedin'] ?>" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
				<?php endif; if($configuracao['redes_sociais_footer_instagram']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_instagram'] ?>" title="Instagram"><i class="fab fa-instagram"></i></a>
				<?php endif; ?>
				</div>
			</div>
			<div class="side-menu">
				<a href="#home">
					<figure class="logo">
						<img src="<?php echo $configuracao['logo_side_menu']['url'] ?>" alt="Logo">
					</figure>
				</a>
				<!-- <ul class="menu">
					<li><a href="#home">Home</a></li>
					<li><a href="#quem-somos">Quem somos</a></li>
					<li><a href="#servicos">Servicos</a></li>
					<li><a href="#clientes">Clientes</a></li>
					<li><a href="#parceiros">Parceiros</a></li>
					<li><a href="#contato">Contato</a></li>
				</ul> -->
				<?php 
				$menu = array(
					'theme_location'  => '',
					'menu'            => 'Menu principal',
					'container'       => false,
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'menu',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 2,
					'walker'          => ''
				);
				wp_nav_menu( $menu );
				?>
				<div class="social">
				<?php if($configuracao['redes_sociais_footer_facebook']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_facebook'] ?>" class="fb" title="Facebook"><i class="fab fa-facebook-f"></i></a>
				<?php endif; if($configuracao['redes_sociais_footer_linkedin']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_linkedin'] ?>" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
				<?php endif; if($configuracao['redes_sociais_footer_instagram']): ?>
					<a href="<?php echo $configuracao['redes_sociais_footer_instagram'] ?>" title="Instagram"><i class="fab fa-instagram"></i></a>
				<?php endif; ?>
				</div>
			</div>
			<div class="conteudo" id="home">
				<div class="principal parallax" style="background-image: url(<?php echo $configuracao['img_fundo']['url'] ?>);">
					<figure class="logo-principal">
						<img src="<?php echo $configuracao['logo_principal']['url'] ?>" alt="Logo">
					</figure>
				</div>
				<div class="porque-aideo">
					<div class="porque-aideo-cont">
						<div class="titulo">
							<h1><?php echo $configuracao['titulo_porque_aideo'] ?></h1>
						</div>
						<div class="txt">
							<p><?php echo $configuracao['texto_porque_aideo'] ?></p>
							<p><?php echo $configuracao['texto_porque_aideo2'] ?></p>
						</div>
					</div>
				</div>
				<div class="sobre-nos" id="quem-somos">


					<?php 
						$contador = 0;
						// EXECUTA O LOOP DE SERVIÇOS DA RESPECTIVA SUBCATEGORIA
                        $quem_somos = new WP_Query(
                             array(
                                'post_type' => 'quem_somos',
                                'posts_per_page' => -1
                            )                                                    
                        );

						while ( $quem_somos->have_posts() ): $quem_somos->the_post();

							$fotoQuemSomos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoQuemSomos = $fotoQuemSomos[0];
							$contador++;

							if(($contador % 2) == 0):

					 	?>
							
							<div class="row">
								<div class="col-md-6">
									<div class="texto">
										<div class="titulo">
											<h2><?php echo get_the_title(); ?></h2>
										</div>
										<div class="cont">
											<p><?php echo get_the_content(); ?></p>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="imagem">
										<figure>
											<img src="<?php echo $fotoQuemSomos ?>" alt="">
										</figure>
									</div>
								</div>
							</div>


						<?php else: ?>

							<div class="row">
								<div class="col-md-6 colDesk">
									<div class="imagem">
										<figure>
											<img src="<?php echo $fotoQuemSomos ?>" alt="">
										</figure>
									</div>
								</div>

								<div class="col-md-6">
									<div class="texto">
										<div class="titulo">
											<h2><?php echo get_the_title(); ?></h2>
										</div>
										<div class="cont">
											<p><?php echo get_the_content(); ?></p>
										</div>
									</div>
								</div>

								<div class="col-md-6 colMobile">
									<div class="imagem">
										<figure>
											<img src="<?php echo $fotoQuemSomos ?>" alt="">
										</figure>
									</div>
								</div>

							</div> 


						<?php endif; endwhile; wp_reset_query(); ?>

				</div>
				<div class="servicos" id="servicos">
					<ul>

						<?php 
						
						// EXECUTA O LOOP DE SERVIÇOS DA RESPECTIVA SUBCATEGORIA
                        $servicos = new WP_Query(
                             array(
                                'post_type' => 'servicos',
                                'posts_per_page' => -1
                            )                                                    
                        );

						while ( $servicos->have_posts() ): $servicos->the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];

					 	?>
							<li>
								<img src="<?php echo $foto; ?>" alt="Imagem serviços">
								<div class="servicos-texto">
									<h2><?php echo get_the_title() ?></h2>
									<p><?php echo get_the_content() ?></p>
								</div>
							</li>

						<?php endwhile; wp_reset_query(); ?>

					</ul>
				</div>
				<div class="clientes" id="clientes">
					<div class="principais-clientes">
						<h2>principais clientes</h2>
					</div>
					<ul class="img-clientes">
						<?php 
						$postClientes = new WP_Query( array('post_type' => 'clientes', 'posts_per_page' => -1) );


						while($postClientes->have_posts()): $postClientes->the_post();

							$fotoCliente = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoCliente = $fotoCliente[0];

							$linkCliente = rwmb_meta('Ideo_link_cliente');

					 	?>

						<li><a href="<?php echo $linkCliente ?>"><img src="<?php echo $fotoCliente ?>" alt=""></a></li>

						<?php endwhile; wp_reset_query(); ?>

					</ul>
					<div class="desc">
						<h5><?php echo $configuracao['desc_clientes'] ?></h5>
					</div>
				</div>
				<div class="parceiros" id="parceiros">
					<div class="principais-parceiros">
						<h2>parceiros</h2>
					</div>
					<ul class="img-parceiros">

						<?php 
						$postsParceiros = new WP_Query( array('post_type' => 'parceiros', 'posts_per_page' => -1) );


						while($postsParceiros->have_posts()): $postsParceiros->the_post();

							$fotoParceiros = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoParceiros = $fotoParceiros[0];

							$linkParceiro = rwmb_meta('Ideo_link_parceiro');

					 	?>


						<li><a href="<?php echo $linkParceiro ?>"><img src="<?php echo $fotoParceiros ?>" alt=""></a></li>

					<?php endwhile; wp_reset_query(); ?>
						
					</ul>
					<div class="desc">
						<h5><?php echo $configuracao['desc_parceiros'] ?></h5>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer() ?>