<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ideo
 */

global $configuracao;

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<!-- META -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta property="og:title" content="" />
		<meta property="og:description" content="" />
		<meta property="og:url" content="" />
		<meta property="og:image" content=""/>
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="" />

		<!-- TÍTULO -->
		<title>IDEO Gestão Tributária</title>

		<!-- FAVICON -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>

	<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
