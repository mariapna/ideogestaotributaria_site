<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_ideogestaotributaria_site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#-UT+*g4Tv-TPYk;8iIe4-G<V]fH2Xb.5E/c7*z[IxQs%V)0ieq_d2jG]FV??mXL');
define('SECURE_AUTH_KEY',  'n?!-acNo){s]|$UX,)N2XSyhVPh8DtnZ!mUj~50E]-`=%:fHgB|Q*$l-5g[hY_X*');
define('LOGGED_IN_KEY',    '@$?Ks}y.0|Tfnu7nS;!la~rzu;l.?>oxdjK@wy).&cl5$?((k6BF`;1YAHw[YlYq');
define('NONCE_KEY',        '7Hfx9X<J|15GqTN`E(l0go2S3]!]aPq(V7~[Q&ez?zvx%|~~IuHo:R&!9avf[8=*');
define('AUTH_SALT',        'RCp`q?~<f6Z)-wz;%AO3PL!F1_j=zlb`38,1V{QIjE8le:$ySyv!!?mK!y7L*K{{');
define('SECURE_AUTH_SALT', '69WAGyfh} m=mHy4xjN&$<X]B OV@7pWN*X--$IH CraA oBe[ +ed+^LNJMmf)v');
define('LOGGED_IN_SALT',   'kvl16T#N2r:L7*AW?]a^|v~UtV*umf)O}D21/DeR/Nz($#b%^x%O7Br]tMs qbI`');
define('NONCE_SALT',       ':$[.-|dI^Hp}7stNug%G?brP4Ci61k `X# L3d{_q;1/,rKLmC.q=F`EbGRvFTUF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'id_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
